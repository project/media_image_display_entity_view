<?php

namespace Drupal\ckeditor_support\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override entity_embed dialog form to add extra options.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('entity_embed.dialog')) {
      $route->setDefault('_form', '\Drupal\ckeditor_support\Form\EntityEmbedDialog');
    }
  }

}
