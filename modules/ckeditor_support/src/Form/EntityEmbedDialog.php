<?php

namespace Drupal\ckeditor_support\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_embed\Form\EntityEmbedDialog as BaseEntityEmbedDialog;

/**
 * Override EntityEmbedDialog form to add image style configuration.
 */
class EntityEmbedDialog extends BaseEntityEmbedDialog {

  /**
   * {@inheritDoc}
   */
  public function buildEmbedStep(array $form, FormStateInterface $form_state): array {
    $form = parent::buildEmbedStep($form, $form_state);
    $entity_element = $form_state->get('entity_element');
    $image_styles = image_style_options(FALSE);
    $form['attributes']['data-entity-embed-image-style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $entity_element['data-entity-embed-image-style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitEmbedStep(array &$form, FormStateInterface $form_state): AjaxResponse {
    $entity_element = $form_state->getValue('attributes');
    $entity_element['data-entity-embed-display-settings']['view_mode'] = $entity_element['data-entity-embed-display'];
    $entity_element['data-entity-embed-display'] = 'media_image_display';
    $form_state->setValue('attributes', $entity_element);
    return parent::submitEmbedStep($form, $form_state);
  }

}
