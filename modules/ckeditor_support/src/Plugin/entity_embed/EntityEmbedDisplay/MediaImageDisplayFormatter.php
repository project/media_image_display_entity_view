<?php

namespace Drupal\ckeditor_support\Plugin\entity_embed\EntityEmbedDisplay;

use Drupal;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_embed\Annotation\EntityEmbedDisplay;
use Drupal\entity_embed\Plugin\entity_embed\EntityEmbedDisplay\EntityReferenceFieldFormatter;
use Drupal\entity_embed\Plugin\entity_embed\EntityEmbedDisplay\ViewModeFieldFormatter;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Embed Display reusing entity reference field formatters.
 *
 * @see \Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayInterface
 *
 * @EntityEmbedDisplay(
 *   id = "media_image_display",
 *   label = @Translation("Media Image Display Entity Embed"),
 *   field_type = "entity_reference"
 * )
 */
class MediaImageDisplayFormatter extends ViewModeFieldFormatter {

  /**
   * Entity_view_display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $entityViewStorage;

  /**
   * EntityDisplayRepository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static{
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityViewStorage = $container->get('entity_type.manager')->getStorage('entity_view_display');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldFormatter(): FormatterInterface|bool {
    $view_mode = $this->fetchViewModeFromAttributes();
    if (!isset($this->fieldFormatter)) {
      $display = [
        'type' => $this->getFieldFormatterId(),
        'settings' => [
          'view_mode' => $view_mode,
        ],
        'label' => 'hidden',
      ];

      // Create the formatter plugin. Will use the default formatter for that
      // field type if none is passed.
      $this->fieldFormatter = $this->formatterPluginManager->getInstance(
        [
          'field_definition' => $this->getFieldDefinition(),
          'view_mode' => '_entity_embed',
          'configuration' => $display,
        ]
      );
    }
    return $this->fieldFormatter;
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $view_mode = $this->fetchViewModeFromAttributes();
    $image_style = $this->fetchImageStyleFromAttributes();
    $build = parent::build();
    $entity = parent::getEntityFromContext();

    if(!$entity instanceof MediaInterface) {
      return $build;
    }

    $image_field_name = $this->defineMediaImageSourceField($entity);
    $entity_type_id = $entity->getEntityTypeId();

    $view_display = $this->entityViewStorage->load($entity_type_id . '.' . $entity->bundle() . '.' . $view_mode);
    if (!$view_display instanceof EntityViewDisplayInterface) {
      $view_display = $this->entityViewStorage->load($entity_type_id . '.' . $entity->bundle() . '.' . 'default');
    }

    if ($view_display instanceof EntityViewDisplayInterface) {
      $components = $view_display->getComponents();
      if(!empty($components[$image_field_name])) {
        $components[$image_field_name]['settings']['image_style'] = $image_style;
        $view_display->setComponent($image_field_name, $components[$image_field_name]);
      }
      $buildEntity = $view_display->build($entity);
      return ['#theme' => 'media_image_display', '#media' => $buildEntity];
    }

    return $build;
  }

  /**
   * Define media image source field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Media entity.
   *
   * @return string|null
   *   Field name if found.
   */
  protected function defineMediaImageSourceField(EntityInterface $entity): ?string {
    if (!$entity instanceof MediaInterface) {
      return NULL;
    }

    $field_definition = $entity->getSource()
      ->getSourceFieldDefinition($entity->bundle->entity);
    $item_class = $field_definition->getItemDefinition()->getClass();
    if ($item_class == ImageItem::class || is_subclass_of($item_class, ImageItem::class)) {
      return $field_definition->getName();
    }
    return NULL;
  }

  /**
   * From attributes, fetch view mode.
   *
   * @return string
   *   View mode define in attributes.
   */
  protected function fetchViewModeFromAttributes(): string {
    $attributes = $this->getAttributeValues();
    $viewMode = $attributes['data-entity-embed-display-settings']['view_mode'];

    $mediaViewModes = $this->entityDisplayRepository->getViewModes('media');
    foreach ($mediaViewModes as $key => $mediaViewMode){
      if('view_mode:'.$mediaViewMode['id'] == $viewMode){
        return $key;
      }
    }
    return '';
  }

  /**
   * From attributes, fetch image style.
   *
   * @return string
   *   Image style define in attributes.
   */
  protected function fetchImageStyleFromAttributes(): string {
    $attributes = $this->getAttributeValues();
    return $attributes['data-entity-embed-image-style'] ?? '';
  }

}
